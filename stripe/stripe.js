const configureStripe = require('stripe')

const ENV = process.env.NODE_ENV || 'development'
const IS_DEV = ENV === 'development'

const STRIPE_SECRET_KEY = IS_DEV ? 'sk_test_sO6Jc7c2DmZtOPoyj19cAfXs' : 'sk_test_sO6Jc7c2DmZtOPoyj19cAfXs'
// 'sk_live_8AvMz4FFKFlgYFQk9bErVuJZ'

const stripe = configureStripe(STRIPE_SECRET_KEY)

module.exports = stripe
