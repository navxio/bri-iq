const ENV = process.env.NODE_ENV || 'development'

const FRONTEND_DEV_URLS = ['http://localhost:3000', 'http://localhost:5000']

const FRONTEND_PROD_URLS = [
  'https://142.93.245.210/',
  'http://142.93.245.210/'
]

module.exports = ENV === 'production'
  ? FRONTEND_PROD_URLS
  : FRONTEND_DEV_URLS
