import React from 'react'
import './SubmitTest.css'

class SubmitTest extends React.Component {
  constructor(props) {
    super(props)
    this.setDetails = this.setDetails.bind(this)
    this.state = {
      first: '',
      last: '',
      email: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.setDetails = this.setDetails.bind(this)
  }

  setDetails() {
    let details = this.state
    let history = this.props.history
    this.props.onClick(details).then(function (response) {
      history.push('/test/payment')
    }).catch(function (e) {
      throw e
    })
  }

  handleChange(event) {
    const name = event.target.name
    const value = event.target.value
    
    this.setState({
      [name]: value
    })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-md-offset-1 unset">
                      <div className="panel setup-content no-padding" id="step-6">
			        	<div className="panel-body no-padding">
                        <div className="col-sm-8 pt-pb-35">
                          <div className="col-sm-offset-4 col-sm-8">
                            <p className="text-medium text-lg">Please save your results below:</p>
                          </div>

                          <div className="clear" style={{margin: "5px 0"}}><br /></div>

			                	<div className="form-group">
                              <div className="col-sm-1">
                              </div>
                              <label for="first_name" className="col-sm-3 control-label">First Name:</label>
                              <div className="col-sm-8">
                                <input type="text" className="form-control input-lg" id="first_name" name="first" value={this.state.first} onChange={this.handleChange} required />

                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-1">
                      </div>
                      <label for="last_name" className="col-sm-3 control-label">Last Name:</label>
                      <div className="col-sm-8">
                        <input type="text" className="form-control input-lg" value={this.state.last} name="last" onChange={this.handleChange} id="last_name" required />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-1">
              </div>
              <label for="email" className="col-sm-3 control-label">Email:</label>
              <div className="col-sm-8">
                <input type="email" className="form-control input-lg" value={this.state.email} name="email" onChange={this.handleChange} id="email"  required />

      {/* <div className="error-msg" >
        <div >{ 'email error' }</div>
      </div >
      <div className="error-msg" >
        <div className="blink_me" >{ 'emailerrornew' }</div>
									</div > */}


							      </div >
							    </div >
      <div className="form-group">
        <div className="col-sm-offset-4 col-sm-8 col-xs-12">
          <button className="btn btn-orange btn-lg pulse-button  btn-lg transition" onClick={this.setDetails}><img className="small-lock" src="/assets/images/lock.png" alt="save my results"/>save my results <img className="small-image" src="/assets/images/arrow-right.png" /></button>
          <span className="android priv-text">we respect your privacy. we don't spam.</span>
        </div>

      </div>


			                </div >
      <div className="col-sm-4 bg-gray pt-pb-35">
        <h3 className="no-margin">why bri<sup>tm</sup> iq test?</h3>


        <table className="table border-bottom" id="quote">
          <tr>
            <td className="middle">
              <img src="/assets/images/80-billion.png" className="step6-icons img-responsive" />
										</td>
              <td className="middle">
                <p className="text-medium text-gray text-lg no-margin">22,910</p>
                <p className="text-gray-l text-small no-margin">iq tests taken</p>
              </td>
									</tr>

            <tr>
              <td className="middle">
                <img src="/assets/images/add-user.png" className="step6-icons img-responsive" />
										</td>
                <td className="middle">
                  <p className="text-medium text-gray text-lg no-margin">20,000+</p>
                  <p className="text-gray-l text-small no-margin">satisfied users</p>
                </td>
									</tr>

              <tr>
                <td className="middle">
                  <img src="/assets/images/like2.png" className="step6-icons img-responsive" />
										</td>
                  <td className="middle">
                    <p className="text-medium text-gray text-lg no-margin">10,000+</p>
                    <p className="text-gray-l text-small no-margin">facebook followers</p>
                  </td>
									</tr>

                <tr>
                  <td className="middle">
                    <img src="/assets/images/match.png" className="step6-icons img-responsive" />
										</td>
                    <td className="middle">
                      <p className="text-medium text-gray text-lg no-margin">200+</p>
                      <p className="text-gray-l text-small no-margin">data points analyzed</p>
                    </td>
									</tr>

                  <tr>
                    <td className="middle">
                      <img src="/assets/images/event.png" className="step6-icons img-responsive" />
										</td>
                      <td className="middle">
                        <p className="text-medium text-gray text-lg no-margin">3 year</p>
                        <p className="text-gray-l text-small no-margin">track record</p>
                      </td>
									</tr>
								</table>



			                </div>


			            </div>
			        </div>
			</div>

        <div className="col-sm-8 col-sm-offset-2">
          <div className="text-white text-center alert-close">
            <img src="/assets/images/alert-icon.png" /> please do not refresh or close this page, or your information may be lost.
				</div>

            <table className="table quote" id="quote1">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">the full report was quite impressive!</p>
                  <p className="designation">zac g.</p>
                </td>
					</tr>
				</table>

            <table  className="table quote" id="quote2">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">quite professional and well organized. loved it!</p>
                  <p className="designation">alex m.</p>
                </td>
					</tr>
				</table>

            <table  className="table quote" id="quote3">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">i thought the strenghts and weaknesses section was spot on.</p>
                  <p className="designation">jason c.</p>
                </td>
					</tr>
				</table>

            <table  className="table quote" id="quote4">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">it was challenging, but i'm happy with the results.</p>
                  <p className="designation">cynthia g.</p>
                </td>
					</tr>
				</table>

            <table className="table quote" id="quote5">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">quick and easy. highly recommended! </p>
                  <p className="designation">jenn z.</p>
                </td>
					</tr>
				</table>

            <table  className="table quote" id="quote6">
					<tr>
              <td className="middle">
                <img src="/assets/images/290-512.png" className="quote-icon" />
						</td>
                <td className="middle">
                  <p className="details">well worth the time and effort.</p>
                  <p className="designation">garrett w.</p>
                </td>
					</tr>
				</table>


          </div>
        </div>
        </div>
        )
      }
    
    }
    
export default SubmitTest
