import React, { Component } from "react";
import Prebody from './Prebody'
import Footer from "./Footer";
import BeginTest from './BeginTest'
import SubmitTest from './SubmitTest'
import Payment from './Payment'
import Quiz from './Quiz'
import { Route, Switch } from 'react-router-dom'
import './Test.css'
let shortid = require('shortid')

class Test extends Component {
  constructor(props) {
    super(props)
    this.state = {
      responses: {},
      quizId: shortid.generate().toUpperCase()
    }

    this.submitResults = this.submitResults.bind(this)
    this.setResponses = this.setResponses.bind(this)
  }

  setResponses(responses) {
    this.setState({responses})
  }
  
  submitResults(details) {
    let data = {
        responses: this.state.responses,
        first: details.first,
        last: details.last,
        quizId: this.state.quizId,
        email: details.email
    }

    // return axios.post('http://142.93.245.210:5000/result', {data}, 
    return fetch('/api/result', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
    
  }

  render() {

    return (
      <div>
        <Prebody />
          <Switch>
            <Route exact path="/test/" component={BeginTest} />
            <Route path="/test/start" render={ (props) => <Quiz {...props} nextStep={this.setResponses} />} />
            <Route path="/test/submit" render={ (props) => <SubmitTest {...props} onClick={this.submitResults} />} />
            <Route path="/test/payment" render={ (props) => <Payment {...props} quizId={this.state.quizId} />} />
          </Switch>
        <Footer />
      </div>
    )
  }
}

export default Test;
