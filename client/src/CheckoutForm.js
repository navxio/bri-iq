import React, { Component } from 'react'
import { CardElement, injectStripe } from 'react-stripe-elements'

class CheckoutForm extends Component {
  constructor(props) {
    super(props)
    this.state = {complete: false}
  }

  async submit(ev) {
    // User clicked submit
    let { token } = await this.props.stripe.createToken({ name: "Name" })
    let response = await fetch("/api/pay", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({source: token.id, quizId: this.props.quizId})
    })

    if (response.ok) alert("Payment successful, reports have been mailed.")
  }

  render() {
    let quizId = this.props.quizId
    return (
      <div className="checkout">
        <p>Would you like to complete the purchase?</p>
        <CardElement />
        <button onClick={this.submit.bind(this)}>Send</button>
      </div>
    )
  }
}

export default injectStripe(CheckoutForm)

