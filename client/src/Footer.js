import React from 'react'
import fbIcon from './assets/images/fb-icon.png'
import gpIcon from './assets/images/google-plus-icon.png'

function Footer(props) {
  return (
    <footer>
      <div className="footer1">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
              <div className="col-sm-6">
                <ul className="footer-left no-margin no-padding">
                  <li> <a href="/">Home</a></li>
                  <li> <a href="terms.html">Terms</a></li>
                  <li> <a href="privacy.html">Privacy</a></li>
                  <li> <a href="disclaimer.html">Disclaimer</a></li>
                  <li> <a href="about.html">About</a></li>
                  <li> <a href="contactus.html">Contact Us</a></li>
                </ul>
              </div>

              <div className="col-sm-6">
                <ul className="footer-right no-margin no-padding">
                  <li> <a href="#" className="transition"> <img src={fbIcon} alt="Facebook Logo" height="26" /></a></li>
                  <li> <a href="#" className="transition"> <img src={gpIcon} alt="Google Plus Logo" height="26" /></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer2">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <p> Copyright © 2018 IQ-Tester.org All rights reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </footer>

  )
}

export default Footer