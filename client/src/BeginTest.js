import React from 'react'
import { Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'

class BeginTest extends React.Component {

  render() {
    const styles = {
      marginTop: '10px',
      marginBottom: '25px'
    }
    return (
      <Modal show={true}>
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
          <h1 className="text-center" style={styles}>The BRI™ Certified IQ Test:</h1>
          <ol>
            <li>There are 20 questions.</li>
            <li>Click on the correct answer choice for each question.</li>
            <li>Take a guess rather than leave a blank.</li>
          </ol>
          <p className="text-center" style={styles}>Good luck! <br /></p>
          <p className="text-center"><Link className="btn btn-danger pulse-button" aria-label="Close" to="/test/start">Start &gt;&gt;  </Link></p>
        </Modal.Body>
      </Modal>
    )
  }
}

export default BeginTest