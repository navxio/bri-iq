import React from 'react'
import { Modal } from 'react-bootstrap'

class Quiz extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      responses: {
        1: '',
        2: '',
        3: '',
        4: '',
        5: '',
        6: '',
        7: '',
        8: '',
        9: '',
        10: '',
        11: '',
        12: '',
        13: '',
        14: '',
        15: '',
        16: '',
        17: '',
        18: '',
        19: '',
        20: ''
      },
      quesId: 1,
      quizFinish: false
    }
    this.nextId = this.nextId.bind(this)
    this.toInfo = this.toInfo.bind(this)
  }

  nextId() {
    this.setState(state => {
      if (state.quesId === 20) {
        return { quizFinish: true}
      } else {
        return { quesId: state.quesId + 1 }
      }
    })
  }

  handleClick(option) {
    this.setState(state => {
      let id = state.quesId
      let responses = state.responses
      responses[id] = option

      if (id === 20) {
        return {
          quizFinish: true,
          responses
        }
      } else {
        return {
          quesId: id + 1,
          responses
        }
      }
    })
  }

  toInfo() {
    this.props.nextStep(this.state.responses)
    this.props.history.push('/test/submit')
  }

  render() {
    const styles = {
      marginTop: '10px',
      marginBottom: '25px'
    }

    return (
      <div id="page-content">
        <div className="container">
          <div className="row">
            <div className="col-md-10 col-md-offset-1 unset">


              <div className="boxed">
                <div className="col-md-12 quiz-form" id="quiz-section">
                  {/* <!-- top area of quiz --> */}
                  <div className="col-md-12 quiz-area-top">
                    {/* <div className="col-md-10">
                          <p style={display: 'none'}>Click the answer choice (A, B, C or D) for each question below.</p>
                        </div> */}
                    <div className="col-md-2">
                      {/* <!-- timer start --> */}
                      <div className="circle">
                        <svg width="80" viewBox="0 0 220 220" xmlns="http://www.w3.org/2000/svg">
                          <g transform="translate(110,110)">
                            <circle r="100" className="e-c-base" />
                            <g transform="rotate(-90)">
                              <circle r="100" className="e-c-progress" />
                              <g id="e-pointer">
                                <circle cx="100" cy="0" r="10" className="e-c-pointer" />
                              </g>
                            </g>
                          </g>
                        </svg>
                      </div>
                      <div className="controlls">
                        {/* <div className="display-remain-time">{{countDown | async | formatTime}}</div> */}
                        <div className="display-remain-time"></div>
                      </div>
                      {/* <!-- timer end --> */}
                    </div>

                    {/* question and option area */}
                    {/* <div className="appQuestion" *ngIf="( now>1 && showQuiz )" > */}
                    <div className="appQuestion" >
                      <div className="col-md-7 quiz-area-middle-left" id="putin">
                        <p>Question {this.state.quesId}/20</p><hr />
                        <div className="col-md-12 question">
                          {/* <img src="{{questionDetail?.image}}" alt="question" className="img-responsive img-center" /> */}
                          <img src={require('./images/Q' + this.state.quesId + '.png')} alt="question" className="img-responsive img-center" />
                        </div>
                      </div>
                      {/* <div  *ngIf="questionDetail"  className="col-md-5 quiz-area-middle-right"> */}
                      <div className="col-md-5 quiz-area-middle-right">
                        <p><b className="choose">Choose an answer:</b> &nbsp;
      <a href="#slide_2" data-toggle="tab" className="pull-right prev_next next_1" onClick={this.nextId}>Skip &gt;&gt;</a>
                        </p>
                        <div className="clearfix"></div><hr />
                        {/* <div className="col-md-6 col-xs-6 col-sm-6 answerArea " [className.blink_me]="(questionDetail?.id == 1)"  *ngFor="let option of questionDetail?.options" > */}
                        <div className="col-xs-6 answerArea" >
                          <div className="row answer-option" >
                            <div className="col-md-4 noright">
                              {/* <img src="/assets/images/{{option.text}}.png" className="img-responsive" /> */}
                              <img src={require('./assets/images/a.png')} className="img-responsive" alt="option" />
                            </div>
                            <div className="col-md-8 noleft">
                              <a onClick={this.handleClick.bind(this, 'a')}>
                                <img className="option-image" src={require('./images/' + this.state.quesId + 'a.png')} alt="option" />
                              </a>
                            </div>
                          </div>
                          <div className="row answer-option" >
                            <div className="col-md-4 noright">
                              <img src={require('./assets/images/b.png')} className="img-responsive" alt="option" />
                            </div>
                            <div className="col-md-8 noleft">
                              <a onClick={this.handleClick.bind(this, 'b')}>
                                <img className="option-image" src={require('./images/' + this.state.quesId + 'b.png')} alt="option" />
                              </a>
                            </div>
                          </div>
                          <div className="row answer-option" >
                            <div className="col-md-4 noright">
                              <img src={require('./assets/images/c.png')} className="img-responsive" alt="option" />
                            </div>
                            <div className="col-md-8 noleft">
                              <a onClick={this.handleClick.bind(this, 'c')}>
                                <img className="option-image" src={require('./images/' + this.state.quesId + 'c.png')} alt="option_c" />
                              </a>
                            </div>
                          </div>
                          <div className="row answer-option" >
                            <div className="col-md-4 noright">
                              <img src={require('./assets/images/d.png')} className="img-responsive" alt="option" />
                            </div>
                            <div className="col-md-8 noleft">
                              <a onClick={this.handleClick.bind(this, 'd')}>
                                <img className="option-image" src={require('./images/' + this.state.quesId + 'd.png')} alt="option" />
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>



                    {/* <div style="" className="qTop">
										
										<span className="message timeup" *ngIf="(now<=0)">Time is UP </span>
										<button type="button" *ngIf="( (now<=0) && showQuiz )" (click)="submitResults()" className="btn btn-danger" tooltip="This will submit your results and show Report to you."> SUBMIT MY ANSWERS </button>

									</div>
									<div  style="display:none" className="StartQuiz">
										<button type="button" *ngIf="( !showQuiz )" className="btn btn-danger" aria-label="Close" (click)="startQuiz()"  >Start Quiz Now ></button>
									</div>
							</div> */}




                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <Modal show={this.state.quizFinish}>
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            <h1 className="text-center" style={styles}>The BRI™ Certified IQ Test:</h1>
            <div> Submit the results here </div>
            <p className="text-center">

              <a className="btn btn-danger pulse-button" onClick={this.toInfo} >Confirm</a>
            </p>
          </Modal.Body>
        </Modal>
      </div>

      // {/* <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openSpatial" id="openSpatialPop" ></button>
      //   <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openMathematical" id="openMathematicalPop" ></button>
      //   <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openPattern" id="openPatternPop" ></button>
      //   <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openVerbal" id="openVerbalPop" ></button> */}

    )
  }

}

export default Quiz