import React, { Component } from 'react'
import Home from './Home'
import What from './What'
import About from './About'
import Test from './Test'
import logo from './assets/images/logo.png'
import {BrowserRouter as Router, Route, NavLink} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import './App.css'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-default">
              <div className="container">
                  <div className="row">
                      <div className="col-md-10 col-md-offset-1">
                          <div className="navbar-header">
                              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                              </button>
                              <NavLink to="/" className="navbar-brand"><img src={logo} alt="Logo"/></NavLink>
                          </div>
                          <div className="navbar-collapse collapse navbar-responsive-collapse">
                              <ul className="nav navbar-nav navbar-left">
                                <li className="active first-link"><NavLink to="/" className="nav-link transition">Home</NavLink></li>
                                <li><NavLink to="/test/" className="nav-link transition">Take the IQ Test</NavLink></li>
                                <li><NavLink to="/what-is-iq" className="nav-link transition">What is IQ?</NavLink></li>
                                <li><NavLink to="/about" className="nav-link transition">About <sup>TM</sup> </NavLink></li>
                              </ul>
                              <ul className="nav navbar-nav navbar-right">
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <Route exact path="/" component={Home}></Route>
          <Route path="/test" component={Test}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/what-is-iq" component={What}></Route>
        </div>

      </Router>
    )
  }
}

export default App
