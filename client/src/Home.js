import React, { Component } from "react";
import laptop from "./assets/images/laptop.png";
import flag from "./assets/images/flag.png";
import lock from "./assets/images/lock.png";
import rangeBar from "./assets/images/range-bar.png";
import review1 from "./assets/images/review1.png";
import review2 from "./assets/images/review2.png";
import review3 from "./assets/images/review3.png";
import review4 from "./assets/images/review4.png";
import review5 from "./assets/images/review5.png";
import review6 from "./assets/images/review6.png";
import review7 from "./assets/images/review7.png";
import review8 from "./assets/images/review8.png";
import review9 from "./assets/images/review9.png";
import Footer from "./Footer";
import "./Home.css";
import { Link } from 'react-router-dom'

class Home extends Component {
  render() {
    return (
      <div>
        <div id="page-header">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <div className="laptop-screen">
                  <img src={laptop} alt="Laptop" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="page-content">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1 unset">
                <div className="information">
                  <p>
                    {" "}
                    <img src={flag} alt="flag" />{" "}
                    <span className="counter">1,976</span> IQ Testers Today | Avg
                    Score: <span className="counter">100</span>
                  </p>
                  <h5> Highly Accurate Online IQ Test </h5>
                  <h6> Test your cognitive skills!</h6>
                  <Link className="pulse-button" to='/test'>
                    {" "}
                    <img className="small-lock" src={lock} alt="lock" /> Find Your
                    IQ score now!
                  </Link>
                  <h4>20 Multiple Choice Questions </h4>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="content">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1 unset">
                <div className="content-head">
                  <h5> IQ Distribution in General Population </h5>
                </div>

                <div className="content-body">
                  <div className="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                    <div className="content-left">
                      <img src={rangeBar} alt="Range-Bar" />
                    </div>
                  </div>

                  <div className="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                    <div className="content-right">
                      <p>
                        {" "}
                        About 144 <span>> Highly Gifted </span>
                      </p>
                      <p>
                        {" "}
                        130-144 <span>> Gifted </span>
                      </p>
                      <p>
                        {" "}
                        114-129 <span>> Above Average </span>
                      </p>
                      <p>
                        {" "}
                        85 - 114 <span>> Average </span>
                      </p>
                      <p>
                        {" "}
                        70 - 84 <span>> Below Average </span>
                      </p>
                      <p>
                        {" "}
                        Below 70 <span>> Lower Extreme </span>
                      </p>
                    </div>
                  </div>
                </div>

                <div className="content-body2">
                  <h5> 1 </h5>
                  <p> Take the quick 20 question IQ Test.</p>
                </div>
                <div className="content-body2">
                  <h5> 2 </h5>
                  <p>
                    {" "}
                    Get your Official IQ Score, along with your IQ Certificate,
                    and a detailed analysis of your strengths and weaknesses.{" "}
                  </p>
                </div>
                <div className="content-body2">
                  <h5> 3 </h5>
                  <p>
                    {" "}
                    Get the practical guide to boosting your IQ, includes 12
                    simple steps to increasing your IQ.
                  </p>
                </div>

                <div className="content-body3">
                  <Link className="pulse-button" to="/test"> Start the Test! </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="new-elements">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-md-offset-1 unset">
                <div className="element1">
                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="pattern1">
                      <p className="tooltip1">
                        <span className="tooltiptext1">
                          Pattern recognition is the ability to see order in a
                          chaotic environment.
                        </span>{" "}
                        <span className="glyphicon glyphicon-ok" /> Pattern
                        Recognition{" "}
                      </p>
                      <p className="tooltip1">
                        <span className="tooltiptext1">
                          Refers to the capacity to think about objects in three
                          dimensions and to draw conclusions about those objects
                          from limited information.
                        </span>{" "}
                        <span className="glyphicon glyphicon-ok" /> Spatial
                        Reasoning{" "}
                      </p>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="pattern1">
                      <p className="tooltip1">
                        {" "}
                        <span className="tooltiptext1">
                          Refers to your ability to understand and see patterns
                          within numbers.
                        </span>{" "}
                        <span className="glyphicon glyphicon-ok" /> Mathematical
                        Ability{" "}
                      </p>
                      <p className="tooltip1">
                        {" "}
                        <span className="tooltiptext1">
                          Refers to the power of comprehension and expression.
                        </span>{" "}
                        <span className="glyphicon glyphicon-ok" /> Verbal
                        Intelligence{" "}
                      </p>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="pattern1">
                      <p className="tooltip1">
                        {" "}
                        <span className="tooltiptext1">
                          Ability to extract deductions from limited supplied
                          information.
                        </span>
                        <span className="glyphicon glyphicon-ok" /> Logical
                        Reasoning{" "}
                      </p>
                      <p className="tooltip1">
                        {" "}
                        <span className="tooltiptext1">
                          classNameification skills enable you to piece together
                          relevant data and make sense out of the whole.
                        </span>
                        <span className="glyphicon glyphicon-ok" /> Classification
                        Skill{" "}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="element2">
                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="counting1">
                      <p> Total number of tests</p>
                      <span className="counter"> 2 </span>
                      <span className="counter"> 2 </span>
                      <span className="counter"> 9 </span>
                      <span className="counter"> 1 </span>
                      <span className="counter"> 0 </span>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="counting1">
                      <p> Number of tests today </p>
                      <span className="counter"> 0 </span>
                      <span className="counter"> 1 </span>
                      <span className="counter"> 9 </span>
                      <span className="counter"> 7 </span>
                      <span className="counter"> 6 </span>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                    <div className="counting1">
                      <p> Today's highest IQ</p>
                      <span className="counter"> 1 </span>
                      <span className="counter"> 6 </span>
                      <span className="counter"> 3 </span>
                    </div>
                  </div>
                </div>

                <div className="element3">
                  <div className="element3-head">
                    <h5> What users are saying</h5>
                    <p>
                      {" "}
                      BRI
                      <sup>TM</sup> Certified IQ Test (iq-tester.org) is the
                      highly rated online IQ Test, taken by thousands of people
                      worldwide. Below are some testimonials from past
                      customers.
                    </p>
                  </div>

                  <div className="element3-data">
                    <div
                      id="carousel-example-generic"
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <ol className="carousel-indicators">
                        <li
                          data-target="#carousel-example-generic"
                          data-slide-to="0"
                          className="active"
                        />
                        <li
                          data-target="#carousel-example-generic"
                          data-slide-to="1"
                        />
                        <li
                          data-target="#carousel-example-generic"
                          data-slide-to="2"
                        />
                      </ol>

                      <div className="carousel-inner" role="listbox">
                        <div className="item active">
                          <div className="slider-content">
                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review1} alt="review1" />
                                    <h5> Richard S.</h5>
                                    <h6 />
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    Wasn't sure what to expect since this is the
                                    first IQ test I've taken in my life. But I'm
                                    glad I took this one. I could see the
                                    different focus areas while taking the test.
                                    Report was detailed.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review2} alt="review2" />
                                    <h5> Sophia R.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    Thanks for the detailed report! I now know
                                    my strengths and weaknesses so I can improve
                                    upon it.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review3} alt="review3" />
                                    <h5> Dennis G.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    I love puzzles and riddles so this was right
                                    up my alley. I got all logical reasoning
                                    questions correct!{" "}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="item">
                          <div className="slider-content">
                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review4} alt="review4" />
                                    <h5> Amelia T.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    Loved the comprehensive report and the tips
                                    on boosting IQ. I'll be trying them.
                                    Impressively put together.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review5} alt="Review5" />
                                    <h5> Andrew S.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    The Verbal Intelligence questions were
                                    tricky!!! But other sections were easier.
                                    Definitely loved the detailed analysis
                                    afterwards thanks.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review6} alt="review6" />
                                    <h5> Natalie H.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    As an ADD patient, the breakdown of test
                                    results really helped me realize which areas
                                    of my brain need improvement. Highly
                                    recommended!{" "}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="item">
                          <div className="slider-content">
                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review7} alt="review7" />
                                    <h5> Dylan A.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    I thought it would be an hour long test but
                                    it's only 20 minutes. I managed to finish it
                                    in 17 minutes.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review8} alt="review8" />
                                    <h5> Samantha D.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    Stumbled upon this website after seeing an
                                    ad online. Fun, informative and
                                    entertaining. Loved it!{" "}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                              <div className="testimonial-main">
                                <div className="testimonial1">
                                  <div className="testimonial-left">
                                    <img src={review9} alt="review9" />
                                    <h5> Sophie F.</h5>
                                    <h6> </h6>
                                  </div>
                                  <div className="testimonial-right">
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                    <span className="glyphicon glyphicon-star rating-fill" />
                                  </div>
                                </div>
                                <div className="testimonial2">
                                  <p>
                                    {" "}
                                    I enjoyed taking the IQ test. The last
                                    couple of questions were more challenging
                                    than I expected and definitely made me think
                                    hard.{" "}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <a
                        className="left carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="prev"
                      >
                        <span
                          className="glyphicon glyphicon-chevron-left"
                          aria-hidden="true"
                        />
                        <span className="sr-only">Previous</span>
                      </a>
                      <a
                        className="right carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="next"
                      >
                        <span
                          className="glyphicon glyphicon-chevron-right"
                          aria-hidden="true"
                        />
                        <span className="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
