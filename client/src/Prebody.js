import React from 'react'
import securityShieldIcon from "./assets/images/security-shield-icon.png";

function Prebody(props) {

  return (
    <div>
      <div id="page-header">
        <div className="container">
          <div className="row">
            <div className="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
              <div className="stepwizard">
                <p className="text-white text-center connection text-medium">
                  <img src={securityShieldIcon} alt='security icon' /> Your connection is 100%
                  Private
                  </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )

}

export default Prebody