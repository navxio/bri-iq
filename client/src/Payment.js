import React, {Component} from 'react'
import './Payment.css'
import CheckoutForm from './CheckoutForm'
import {Elements, StripeProvider} from 'react-stripe-elements'


class Payment extends React.Component {

	render() {

		return (
			<StripeProvider apiKey="pk_test_QjeJmPDUMXR9ATmmbm6LhN9u">
			<div id="page-content">
				<div className="container">

					<div className="row">
						<div className="col-md-10 col-md-offset-1 unset">
							<div className="row">
								<div className="col-md-7 left-content">
									<div className="d-reverse">


										<div className="bri-box bg-w">

											<h3 className="bg-2 new-c">LAST STEP</h3>
											<div className="bri-boxWrap">
												<ul className="purchase-list left-cnt-list">
													<li>
														<div className="d-flex">
															<div className="list-content"><div className="mn-titlee" style={{ color: "#f1693c" }}> <span className="nmber">1.</span>IQ Score Evaluation</div>
																<p className="pra">Your comprehensive IQ Score</p>
															</div>
															<div className="media-item">
																<img className="img-responsive" src="/assets/images/iqscore.png" alt="iqscore logo" />
															</div>
														</div>
													</li>
													<li>

														<div className="d-flex">
															<div className="list-content"><div className="mn-titlee" style={{ color: "#f1693c" }}> <span className="nmber">2.</span>Your IQ Certificate</div>
																<p className="pra">With Date, Official Serial # and Logo</p>
															</div>
															<div className="media-item">
																<img className="img-responsive" src="/assets/images/cert_mini.png" />
															</div>
														</div>
													</li>
													<li>

														<div className="d-flex">
															<div className="list-content"><div className="mn-titlee" style={{ color: "#f1693c" }} > <span className="nmber">3.</span>Full Performance Report</div>
																<p className="pra lst-pra">16-page analysis of your cognitive strengths and weaknesses</p>
															</div>
															<div className="media-item last-imgss">
																<img className="img-responsive" src="/assets/images/report-thumb.png" alt="report-thumbnail" />
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div className="bri-box bg-w">

										<div className="bri-boxWrap tin-brd">
											<div className="tabbable-panel">

												<div className="tabbable-line" >
													<div >
														<div className="alert alert-success">
															We have already sent you email for this Quiz! Please contact <a href="mailto:contact@iq-tester.org">contact@iq-tester.org</a> if haven't received yet!
												</div>
													</div>
												</div>

												<div className="tabbable-line" >

													<div className="tab-content">

														<div className="big-tp-tittle">

															<h1 align="right">Total: <span className="normal" style={{ color: "#666675" }}>&nbsp; &nbsp;{'symbol'}{'paypalAmount'}</span></h1>
															<span className="cards-img"><img src="https://iq-tester.org/assets/images/visa-mc-amex.png" /></span>
														</div>

														<div className="tab-pane animated slideInUp active" id="tab_default_1">
															<div id="payment-form">

																<div className="text-center">
																	<div>
																		<div className="alert alert-success">
																			<strong>Success!</strong> Your Payment has been received. Your Transaction ID is : {'transactionID'}
																		</div>
																	</div>

																	<div >
																		<div className="alert alert-success">
																			<strong>Success!</strong> We have emailed your reports to your email ID!
																	</div>
																	</div>
																	<div >
																		<div className="alert alert-danger">
																			<strong>Wait!</strong> Please don't reload while we complete your request!
																	</div>
																	</div>


																	<div >
																		<div className="alert alert-success">
																			{'success'}
																		</div>
																	</div>


																	<button className="btn btn-orange btn-lg pulse-button "  >

																		<img className="small-lock" src="/assets/images/lock.png" />
																		Continue
                                                                <img className="small-image" src="/assets/images/arrow-right.png" />
																	</button>





																	<div className="checkout-section " >

																		<div className="upper row">
																			<div className="upper-left">

																				<img src="/assets/images/card.png" />
																			</div>
																			<div className="upper-right">

																				<img src="/assets/images/padlock.png" />
																			</div>
																		</div>


																		<div className="cell example example1">

																			<div >
																				<div id="payment-request-button" >
																				</div>
																			</div>





																			<div className="cell example example3">
																				<StripeProvider apiKey="pk_test_QjeJmPDUMXR9ATmmbm6LhN9u">
																					<div className="example">
																						<Elements>
																							<CheckoutForm quizId={this.props.quizId} />
																						</Elements>
																					</div>
																				</StripeProvider>

			{/* PAYMENT FROM BEGINS */}
																				<form >

																					<div className="group">
																						{/* <label>
																							<span>Name</span>
																							<input id="cardholder-name" name="name" className="field" placeholder="Click Here to Enter Name" required="" value="{{userName}}" />
																						</label>
																						<label>
																							<span>Card Number</span>
																							<div id="example3-card-number" className="field"></div>
																						</label>
																						<label>
																							<span>Expiry </span>
																							<div id="example3-card-expiry" className="field"></div>
																						</label>
																						<label>
																							<span>CVV Code</span>
																							<div id="example3-card-cvc" className="field"></div>
																						</label> */}

																					</div>
																					<div className="error" role="alert">
																						<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
																							<path className="base" fill="#000" d="M8.5,17 C3.80557963,17 0,13.1944204 0,8.5 C0,3.80557963 3.80557963,0 8.5,0 C13.1944204,0 17,3.80557963 17,8.5 C17,13.1944204 13.1944204,17 8.5,17 Z"></path>
																							<path className="glyph" fill="#FFF" d="M8.5,7.29791847 L6.12604076,4.92395924 C5.79409512,4.59201359 5.25590488,4.59201359 4.92395924,4.92395924 C4.59201359,5.25590488 4.59201359,5.79409512 4.92395924,6.12604076 L7.29791847,8.5 L4.92395924,10.8739592 C4.59201359,11.2059049 4.59201359,11.7440951 4.92395924,12.0760408 C5.25590488,12.4079864 5.79409512,12.4079864 6.12604076,12.0760408 L8.5,9.70208153 L10.8739592,12.0760408 C11.2059049,12.4079864 11.7440951,12.4079864 12.0760408,12.0760408 C12.4079864,11.7440951 12.4079864,11.2059049 12.0760408,10.8739592 L9.70208153,8.5 L12.0760408,6.12604076 C12.4079864,5.79409512 12.4079864,5.25590488 12.0760408,4.92395924 C11.7440951,4.59201359 11.2059049,4.59201359 10.8739592,4.92395924 L8.5,7.29791847 L8.5,7.29791847 Z"></path>
																						</svg>
																						<span className="message blink_me">{'error'}</span>
																					</div>

																					<div className="secure-text">
																						<span className="message "><img src="/assets/images/padlock.png" /> 100% Secure Checkout Guaranteed</span>
																					</div>

																					<button className="btn btn-orange btn-lg pulse-button" data-tid="elements_examples.form.pay_button" type="submit "><img className="small-lock" src="/assets/images/lock.png" /> GET MY IQ RESULT<img className="small-image" src="/assets/images/arrow-right.png" /></button>
																				</form>


																			</div>



																			<div className="col-md-12 text-center pp-text android">Prefer <img src="/assets/images/PP_logo_h_100x26.png" />? <span className="under_paypal"> <div id="Paypal"><div id="paypal-button"></div></div> </span></div>

																		</div>



																	</div>



																	<div className="lower-left no-popup" >
																		<div className="row">
																			<div className="col-md-12">
																				<div className="col-sm-6"><i className="fire hide">🔥</i><img src="/assets/images/check.png" /> <span> Over 100,000 IQ Tests Taken</span></div>
																				<div className="col-sm-6"><img src="/assets/images/check.png" /> <span> No additional charges</span></div>
																				<div className="col-sm-6"><img src="/assets/images/check.png" /> <span> 100% Satisfaction Guaranteed</span></div>
																				<div className="col-sm-6"><img src="/assets/images/check.png" /> <span> Live Support: 800-438-6926</span></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="pl-icon sec hide secure-icon">
															<img src="/assets/images/securelogo.png" alt="" />
														</div>
														<div className="row">
															<div className="col-md-12">
																<div className="pl-icons col-xs-12"  >
																	<img src="https://iq-tester.org/assets/images/dual-logo.png" alt="" />
																</div>
															</div>
														</div>

													</div>
												</div>
											</div>

										</div>

									</div>

								</div>
								<div className="col-md-5" id="right-content">
									<div className="bri-box bg-w">




										<div className="bri-box bg-w">

											<h3 className="bg-4">What Customers Are Saying...</h3>
											<div className="bri-boxWrap">

												<ul className="order-summary">
													<li>
														"Excellent - Got result immediately!"
											</li>
													<li>
														"The report was very professional."
											</li>
													<li>
														"Performance breakdown in various areas was helpful."
											</li>
													<li>
														"Was hoping it was free; but after paying I realize it's worth it."
											</li>
													<li>
														"eBook was great. Had solid tips."
											</li>

												</ul>

											</div>
										</div>



										<div className="bri-box bg-w">

											<h3 className="bg-4">Billing FAQ</h3>
											<div className="bri-boxWrap">
												<h4 className="order-heading">
													Why isn't this all free?
										</h4>
												<p>Developing, hosting and advertising this IQ Test requires a sustainable revenue source. This project would not be possible without it.</p>
												<h4 className="order-heading">
													Is this secure?
										</h4>
												<p>Yes it is - 100%. This website uses SSL to encrypt your information. Your payment information is securely handled by Stripe. You may use Paypal for as your payment method as well.</p>
												<h4 className="order-heading">
													What happens after I pay?
										</h4>
												<p>After you pay, you will be emailed your IQ Score, Your IQ Test Results and the eBook titled "Practical Guide on Boosting IQ."</p>

											</div>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-10">
							<div className="above-footer-text text-center">

								<a routerLink="/"> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</StripeProvider>
		)
	}
}

export default Payment
