import React, {Component} from 'react'
import OptionArray from './OptionArray'

class Question extends Component {

  constructor(props) {
    super(props)
  }

  return (
    <div className="col-md-7 quiz-area-middle-left">
      <p>Question 3/20</p><hr>
        <div classNameName="col-md-12 question">
          <img src="{{questionDetail?.image}}" alt="question" className="img-responsive img-center" />
	      </div>
        </div>
        <div className="col-md-5 quiz-area-middle-right">
          <p><b className="choose">Choose an answer:</b> &nbsp;
	<a href="#slide_2" data-toggle="tab" className="pull-right prev_next next_1" >Skip &gt;&gt;</a></p>
          <div className="clearfix"></div><hr>

            <div className="col-md-6 col-xs-6 col-sm-6 answerArea " >
		<div className="row answer-option" >
              <div className="col-md-4 noright">
                <img src="/assets/images/{{option.text}}.png" className="img-responsive">
			</div>
                <div className="col-md-8 noleft">
                  <img className="option-image" src="what?" alt="option" />
                </div>
              </div>
            </div>
	
</div>

          <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openSpatial" id="openSpatialPop" ></button>
          <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openMathematical" id="openMathematicalPop" ></button>
          <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openPattern" id="openPatternPop" ></button>
          <button type="button" className="btn btn-info dontshow" data-toggle="modal" data-target="#openVerbal" id="openVerbalPop" ></button>	
 
  )
}

export default Question