const jwt = require('jsonwebtoken')

function verifyToken (req, res, next) {
  const token = req.headers['x-access-token']
  if (!token) return res.status(401).send({ status: 401, message: 'No token provided.' })

  jwt.verify(token, process.env.SECRET, function (err, decoded) {
    if (err) return res.status(500).send({ status: 500, message: 'Failed to authenticate token.' })
    if (decoded.id === 'al@playpuddle.com') {
      next()
    } else {
      return res.status(401).send({ status: 401, message: 'Unauthorized' })
    }
  })
}

module.exports = verifyToken
