const express = require('express')
const bodyParser = require('body-parser')
let app = express()
const sgMail = require('@sendgrid/mail')
const path = require('path')
const pdffiller = require('pdffiller')
const fs = require('fs')
const results = require('./results')
const mongoose = require('mongoose')
const mongooseUri = process.env.MONGODB_URI || 'mongodb://localhost/bri-dev'
let QuizResponse = require('./config')
const { adjectives, percentiles } = require('./methods')
const ENV = process.env.NODE_ENV || 'development'
const cors = require('cors')
// const CORS_WL = require('./stripe/core_wl')
const stripe = require('./stripe/stripe')
const { promisify } = require('util')
const readFileAsync = promisify(fs.readFile)
const fillFormAsync = promisify(pdffiller.fillForm.bind(pdffiller))

// const corsOptions = {
//   origin: (origin, callback) => {
//     console.log('the origin is ' + origin)
//     return (CORS_WL.indexOf(origin) !== -1)
//       ? callback(null, true)
//       : callback(new Error('Not allowed by CORS'))
//   }
// }
if (ENV === 'development') {
  require('dotenv').config()
}

sgMail.setApiKey(process.env.SENDGRID_API_KEY)
console.log(process.env.SENDGRID_API_KEY)

mongoose.connect(mongooseUri)

const db = mongoose.connection

db.on('error', () => {
  throw new Error('Unable to connect to database at ' + mongooseUri)
})

// CAN I REMOVE THIS?
// app.use(cors(corsOptions))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/', express.static(path.join(__dirname, 'client/build')))

app.post('/api/result', function (req, res) {
  let ipAddress = req.connection.remoteAddress
  console.log(req.body)
  let { responses, email, first, last, quizId } = req.body
  const score = results(responses)

  // calculate score
  let total = 0
  for (let key in score) {
    if (score.hasOwnProperty(key)) {
      total += score[key]
    }
  }
  total *= 7
  let q = new QuizResponse({
    _id: quizId,
    first,
    last,
    email,
    responses,
    iqScore: total,
    paid: false,
    emailSent: false,
    ipAddress
  })

  q.save(function (err, result) {
    if (err) return res.status(500).send({ status: 500, message: 'Could not save to the database' })
    return res.status(200).send()
  })
})

app.post('/api/pay', async (req, res) => {
  const checkoutIPAddress = req.connection.remoteAddress
  const quizId = req.body.quizId
  try {
    let { status } = await stripe.charges.create({
      amount: 1000,
      currency: 'usd',
      description: 'an example charge',
      source: req.body.source
    })
    if (status === 'succeeded') {
      // payment has been successful, write this to db
      await QuizResponse.findByIdAndUpdate(quizId, { paid: true, checkoutIPAddress }).exec()
      let q = await QuizResponse.findById(quizId).exec()
      let { responses, email, first, last, iqScore } = q
      const name = first + ' ' + last
      const score = results(responses)
      const total = iqScore
      let sourcePDF = 'pdf_templates/Certificate.pdf'
      let destinationPDF = 'certificate.pdf'
      let data = {
        name,
        quiz_version: '1.0',
        quiz_serial: q._id,
        score: total,
        test_date: new Date().toLocaleDateString()
      }
      await fillFormAsync(sourcePDF, destinationPDF, data)
      sourcePDF = 'pdf_templates/Report.pdf'
      destinationPDF = 'report.pdf'
      data = {
        quiz_version: '1.0',
        serial: quizId,
        name,
        iq_score: total,
        test_date: new Date().toLocaleDateString(),
        verbal_total_copy: score['verbal'] * 7,
        verbal_percentile: adjectives(score['verbal'])[1],
        mathematical_total: score['mathematical'] * 7,
        mathematical_total_copy: score['mathematical'] * 7,
        mathematical_considered: adjectives(score['mathematical'])[0],
        mathematical_percentile: adjectives(score['mathematical'])[1],
        verbal_considered: adjectives(score['verbal'])[0],
        spatial_total: score['spatial'] * 7,
        spatial_total_copy: score['spatial'] * 7,
        spatial_considered: adjectives(score['spatial'])[0],
        spatial_percentile: adjectives(score['spatial'])[1],
        logical_total: score['logical'] * 7,
        logical_total_copy: score['logical'] * 7,
        logical_considered: adjectives(score['logical'])[0],
        logical_percentile: adjectives(score['logical'])[1],
        pattern_total: score['pattern'] * 7,
        pattern_total_copy: score['pattern'] * 7,
        pattern_considered: adjectives(score['pattern'])[0],
        verbal_total: score['verbal'] * 7,
        pattern_percentile: adjectives(score['pattern'])[1],
        overall_iq: total,
        percent_population: percentiles(total),
        verbal_intelligence: score['verbal'] * 7,
        mathematical_ability: score['mathematical'] * 7,
        spatial_reasoning: score['spatial'] * 7,
        logical_reasoning: score['logical'] * 7,
        pattern_recognition: score['pattern'] * 7
      }
      await fillFormAsync(sourcePDF, destinationPDF, data)
      let certificate = (await readFileAsync('./certificate.pdf')).toString('base64')
      let report = (await readFileAsync('./report.pdf')).toString('base64')
      const msg = {
        to: email,
        from: 'y2kmaster@gmail.com',
        subject: 'Reports and Certificate',
        text: 'Please find attached your report for the IQ test',
        attachments: [
          {
            content: certificate,
            filename: 'Certificate.pdf'
          },
          {
            content: report,
            filename: 'Report.pdf'
          }
        ]
      }
      sgMail.send(msg)
      console.log('Email Sent')
      await QuizResponse.findByIdAndUpdate(quizId, { emailSent: true }).exec()
      res.json({ status })
    } else {
      res.status(500).end()
    }
  } catch (err) {
    res.status(500).end()
  }
})

app.use((req, res, next) => {
  return res.status(404).send({ status: 404, message: 'Endpoint not found.' })
})

module.exports = app
