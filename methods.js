module.exports = {
  adjectives: function (score) {
    switch (score) {
      case 0:
      case 1:
        return ['Below Average', '25']
      case 2:
        return ['Average', '50']
      case 3:
        return ['Good', '65']
      case 4:
        return ['Awesome', '80']
    }
  },
  percentiles: function (totalScore) {
    switch (true) {
      case (totalScore < 40):
        return 75
      case (totalScore < 70):
        return 50
      case (totalScore < 100):
        return 25
      case (totalScore < 140):
        return 10
    }
  }
}
