const mongoose = require('mongoose')
const { isEmail } = require('validator')
let Schema = mongoose.Schema

const quizResponseSchema = new Schema({
  _id: Schema.Types.String,
  name: {
    type: Schema.Types.String
  },
  email: {
    type: Schema.Types.String,
    required: true,
    validate: {
      validator: function (input) {
        return isEmail(input)
      }
    }
  },
  responses: {
    type: Schema.Types.Mixed,
    required: true
  },
  first: {
    type: Schema.Types.String,
    required: true
  },
  last: {
    type: Schema.Types.String,
    required: true
  },
  iqScore: Schema.Types.Number,
  paid: Schema.Types.Boolean,
  emailSent: Schema.Types.Boolean,
  ipAddress: Schema.Types.String,
  checkoutIPAddress: Schema.Types.String
}, { timestamps: true })

module.exports = mongoose.model('QuizResponse', quizResponseSchema)
