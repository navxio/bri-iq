const answers = {
  1: 'c',
  2: 'c',
  3: 'a',
  4: 'd',
  5: 'c',
  6: 'd',
  7: 'd',
  8: 'b',
  9: 'c',
  10: 'a',
  11: 'd',
  12: 'c',
  13: 'a',
  14: 'c',
  15: 'c',
  16: 'd',
  17: 'c',
  18: 'd',
  19: 'b',
  20: 'c'
}

module.exports = function (responses) {
  let accumulator = {
    verbal: 0,
    mathematical: 0,
    spatial: 0,
    pattern: 0,
    logical: 0
  }

  for (let id in responses) {
    if (responses.hasOwnProperty(id)) {
      id = parseInt(id)
      if (id > 0 && id < 5) {
        if (responses[id] === answers[id]) {
          accumulator.logical++
        }
      } else if (id > 4 && id < 9) {
        if (responses[id] === answers[id]) {
          accumulator.spatial++
        }
      } else if (id > 8 && id < 13) {
        if (responses[id] === answers[id]) {
          accumulator.mathematical++
        }
      } else if (id > 12 && id < 17) {
        if (responses[id] === answers[id]) {
          accumulator.pattern++
        }
      } else if (id > 17 && id < 21) {
        if (responses[id] === answers[id]) {
          accumulator.verbal++
        }
      }
    }
  }
  return accumulator
}
